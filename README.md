# Some (_quick_) notes about FreedomPhone software
**_Last Updated :_**
- **_April 17th, 2022_**     - _Starting... WIP_

\
The **Freedom Phone** is a smartphone, that - [_according to its promoters' claims_](https://www.freedomphone.com/) - should be a **"free speech and privacy first focused"** device.

It was officially launched on July 14th 2021 via a social media campaign, that was aimed in particular to conservative users.

However the shipments of the first units to buyers only started in late January 2022... so in the end kitting+shipping operations involved less than 8000 units, owing to order cancellations during such 6+ months = refund requests + psp cancellations.

--------------

### Index

- 1. [Freedom Phone Specifications](#1-freedom-phone-specifications)

- - 1.1 [Advertised Samples](#11-advertised-samples)

- - 1.2 [Retail Units](#12-retail-units)

- 2. [ClearOS](#2-clearos)

- 3. [ClearApps](#3-clearapps)

-----------------

## 1. Freedom Phone Specifications

### 1.1. Advertised Samples

https://twitter.com/relationsatwork/status/1415486911716675585

WIP...

-----------------

### 1.2. Retail Units

https://twitter.com/relationsatwork/status/1424806945500930053

https://twitter.com/relationsatwork/status/1424806958251614211

WIP...

------------------

## 2. ClearOS

https://twitter.com/relationsatwork/status/1424806976786214916

WIP...
